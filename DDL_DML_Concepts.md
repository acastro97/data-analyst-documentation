# DDL and DML Concepts and Functions

**This document has as a main goal provide information to the reader about the DML and DDL concepts and which are they respective functions. This with the objetive to help them to understand and increase their knowledge about databases.**

## DML (Data Modification Language)

**DML is a group of instructions with the ability to modify the data into the database, this group is composed by:**

* ## <b>Insert</b>
   - This function will allow us to insert new registers into an specific table.
     * ``` INSERT INTO clients VALUES ('Allan', 'Romero', '207660941') ```
  <br>
* ## <b>Update</b>
   - This function will allow us to update registers where the stipulated clause is fulfilled from an specific table.
     * ``` UPDATE clients SET [name = 'Rigoberto', lastname = 'Duran'] WHERE id = 1 ( Estipulated Clause ) ) ```
  <br>
* ## <b>Delete</b>
  - This function will allow us to delete registers where the stipulated clause is fulfilled from an specific table.
     * ``` DELETED FROM clients WHERE id = 1 ( Estipulated Clause ) ```
  <br><br> 
* ## <b>Joins:</b> 
      
    The join function is used to extract data from different tables: <br><br>

   - <b>Inner Join:</b> This kind of join will extract the values that are related between the 2 tables by a column in common.<br>     
      ![Inner Join](/Images/inner_join.png "Inner Join")
      <br><br>    
   - <b>Left Join:</b> This kind of join will extract all the data from the table <b>"A"</b> and will show the values of the table <b>"B"</b>, if doesn't exist any coincidence between tables the output will be <b>"Null"</b>. <br><br>
      ![Left Join](/Images/left_join.png "Left Join")
      <br><br>
   - <b>Right Join:</b> This kind of join will extract all the data from the table <b>"B"</b> and will show the values of the table <b>"A"</b>, if doesn't exist any coincidence between tables the output will be <b>"Null"</b>.<br><br>
      ![Right Join](/Images/right_join.png "Right Join")  
      <br><br>
   - <b>Full Join:</b> This join will take all the values from the table <b>"A"</b> and <b>"B"</b> no matter if don't exist any relation between them.
      <br><br>
      ![Right Join](/Images/full_join.png "Right Join")  
      <br>
    
## DDL ( Data Definition Language)  

**DDL or Data  Definition Language is a group of sentences that are used to define the database structure.**
**hese sentences or functions are:**

* CREATE
  - This function will allow us to create structures like tables databases etc:
    *  ``` CREATE TABLE client ( id INT PRIMARY KEY, name VARCHAR(50), weight INT) ```
* ALTER
  - This function will allow us to modify the tables structure:
     * Adding a new column: ``` ALTER TABLE client ADD height DECIMAL ``` 
     * Updating an existing column: ``` ALTER COLUMN weight DECIMAL ``` 
* DROP
  - This function is to delete the existing database or any object within this database
    * Deleting a database: ``` DROP DATABASE Microsoft_DB  ```
    * Deleting an specific table column: ``` ALTER TABLE client DROP COLUMN name ```
    * Deleting a table: ``` DROP TABLE client ```
* TRUNCATE
  - This function is used to remove all the records from an specific table
    * Removing records from a unrelated table: ``` TRUNCATE TABLE client ```
    * Removing records from a related table: ``` TRUNCATE TABLE client CASCADE ```
